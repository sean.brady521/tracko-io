import { IonPage, IonButton, IonFab, IonFabButton, IonIcon, IonItem, IonText } from '@ionic/react'
import CategoryCard from 'components/category-card'
import { add } from 'ionicons/icons'
import React  from 'react'
import { ICategory } from 'types/common'
import styles from './track.module.scss'

export interface TrackProps {
  categories: ICategory[]
  categoryTotals: { [categoryId: string]: number }
  onAddTransaction: () => void
}

const Track = (props: TrackProps) => {
  const { categories = [], categoryTotals, onAddTransaction } = props

  return (
    <IonPage className={styles.root}>
      <div className={styles.headerSection}>
        <div className={styles.headersWrap}>
          <IonText color='dark'>
            <div className={styles.header}>
              Categories
            </div>
          </IonText>
          <IonText color='medium'>
            <div className={styles.subHeader}>
              Monthly spending
            </div>
          </IonText>
        </div>
        <IonButton 
          fill='clear' 
          className={styles.addButton} 
          shape='round' 
          strong
          routerLink='/edit-category'
          routerDirection='none'
        >
          Add
        </IonButton>
      </div>
      <div className={styles.categories}>
        {categories.map(cat => (
          <CategoryCard 
            className={styles.card} 
            key={cat.id}
            icon={cat.icon}
            name={cat.name}
            amount={categoryTotals[cat.id] || 0}
            routerLink={`/category-transactions?categoryId=${cat.id}`}
          />
        ))}
      </div>
      <IonFab 
        horizontal='end'
        vertical='bottom'
        className={styles.fab}
      >
        <IonFabButton onClick={onAddTransaction} routerDirection='none'>
          <IonIcon icon={add} />
        </IonFabButton>
      </IonFab>
    </IonPage>
  ) 
}

export default Track
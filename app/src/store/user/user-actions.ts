import firebase from 'firebase/app'
import 'firebase/auth'
import { checkEmailSignIn } from 'utils/auth'
import { Dispatch } from 'redux'
import { replace } from 'connected-react-router'

export const setLoggedIn = (loggedIn: boolean) => ({
  type: 'SET_LOGGED_IN',
  payload: { loggedIn }
})

export const setLoading = (loading: boolean) => ({
  type: 'SET_LOADING',
  payload: { loading }
})

export const setError = (error: string) => ({
  type: 'SET_ERROR',
  payload: { error }
})

export const setShowNav = (showNav: boolean) => ({
  type: 'SET_SHOW_NAV',
  payload: { showNav }
})

export const initAuthManager = (onLogin?: () => void) => {
  return async (dispatch: Dispatch) => {
    await checkEmailSignIn(() => { 
      dispatch(replace('/'))
    })
    firebase.auth().onAuthStateChanged(user => {
      dispatch(setLoggedIn(!!user))
      dispatch(setLoading(false))
      if (!user) dispatch(setError('401: login failed'))
      if (user && onLogin) onLogin()
    })
  }
}

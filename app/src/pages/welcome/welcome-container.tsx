import React, { useEffect } from 'react'
import Welcome  from './welcome'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'
import { setShowNav } from 'store/user/user-actions'

interface WelcomeContainerProps {
  setShowNav: (show: boolean) => void
}

export const WelcomeContainer = ({...props }: WelcomeContainerProps) => {
  const { setShowNav } = props

  useEffect(() => {
    setShowNav(false)
  }, [setShowNav])

  return (
    <Welcome {...props} />
  )
}

export function mapStateToProps(state: StoreState) {}

export function mapDispatchToProps(dispatch: any) {
  return { 
    setShowNav: (show: boolean) => dispatch(setShowNav(show)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeContainer)
import React, { useState } from 'react'
import { IonButton, IonIcon, IonInput } from '@ionic/react'
import PageWrapper from 'components/page-wrapper'
import { basket } from 'ionicons/icons'
import styles from './manage-category.module.scss'

export interface ManageCategoryProps { 
  type: 'Add' | 'Edit'
  defaultCategory?: string
  onSave: (name: string, icon: string) => void
}

const ManageCategory = (props: ManageCategoryProps) => {
  const { type, defaultCategory, onSave } = props

  const [name, setName] = useState<string>(defaultCategory || '')
  const [icon, setIcon] = useState<string>(basket || '')

  return (
    <PageWrapper 
      title={`${type} category`} 
      backLink='/categories' 
    >
      <div className={styles.contentWrap}>
        <div className={styles.iconSelectWrap}>
          <IonIcon icon={icon} className={styles.iconSelect} color='dark' />
        </div>
        <div className={styles.categoryNameWrap}>
          <IonInput 
            value={name} 
            autofocus
            placeholder='Type category name'
            maxlength={20}
            className={styles.nameInput}
            onIonChange={e => setName(e.detail.value!)}
            color='primary'
          />
        </div>
        <div className={styles.saveWrap}>
          <IonButton
            shape='round'
            disabled={!name}
            strong
            routerLink='/categories' 
            routerDirection='none'
            onClick={() => onSave(name, icon)}
            color={name ? 'primary' : 'medium'}
            className={styles.saveButton}
          >
            Save
          </IonButton>
        </div>
      </div>
    </PageWrapper>
  ) 
}

export default ManageCategory
import React, { useEffect } from 'react'
import ManageDescription, { ManageDescriptionProps } from './manage-description'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'
import { setShowNav } from 'store/user/user-actions'
import parseLocation from 'utils/parse-location'
import { updateEditingTransaction } from 'store/transactions/transactions-actions'
import { RouteComponentProps } from 'react-router'

interface CategoriesContainerProps extends ManageDescriptionProps, RouteComponentProps { 
  setShowNav: (show: boolean) => void
}

export const ManageDescriptionContainer = ({...props }: CategoriesContainerProps) => {
  const { setShowNav } = props

  useEffect(() => {
    setShowNav(false)
  }, [setShowNav])

  return (
    <ManageDescription {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { editingTransaction } = state.transactions
  const { tranId } = parseLocation(window.location)

  let backLink = '/edit-transaction'
  if (tranId) backLink += `?tranId=${tranId}`


  return {
    backLink,
    defaultDescription: editingTransaction.description,
  }
}

export function mapDispatchToProps(dispatch: any, ownProps: CategoriesContainerProps) {
  const { tranId } = parseLocation(window.location)

  const { history } = ownProps

  return { 
    setShowNav: (show: boolean) => dispatch(setShowNav(show)),
    onSave: (description: string) => {
      dispatch(updateEditingTransaction({
        id: tranId,
        description
      }))

      let backLink = '/edit-transaction'
      if (tranId) backLink += `?tranId=${tranId}`
      history.push(backLink, 'none')
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageDescriptionContainer)
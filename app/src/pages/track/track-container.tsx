import React, { useEffect } from 'react'
import Track, { TrackProps } from './track'
import { connect } from 'react-redux'
import { StoreState, ITransaction } from 'types/common'
import { postTransaction, removeTransaction, setEditingTransaction } from 'store/transactions/transactions-actions'
import { push } from 'connected-react-router'
import { setShowNav } from 'store/user/user-actions'
import { RouteComponentProps } from 'react-router'
import { selectTotalsByCategory } from 'store/transactions/transactions-selectors'

interface TransactionsContainerProps extends TrackProps, RouteComponentProps {
  forceShowNav: () => void
  showNav: boolean
  loggedIn: boolean
}

export const TransactionsContainer = ({ forceShowNav, showNav, loggedIn, history,  ...props }: TransactionsContainerProps) => {
  useEffect(() => {
    if (!loggedIn) {
      history.push('/signin')
    }
    if (!showNav) forceShowNav()
  }, [forceShowNav, showNav, loggedIn, history])

  return (
    <Track {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { loggedIn, showNav } = state.user
  const { transactions, loading, error } = state.transactions
  const { categories } = state.categories

  return {
    categories,
    categoryTotals: selectTotalsByCategory(transactions),
    showNav,
    loading,
    loggedIn,
    authenticating: state.user.loading,
    error,
  }
}

export function mapDispatchToProps(dispatch: any, ownProps: TransactionsContainerProps) {
  const { history } = ownProps
  return {
    onAddTransaction: () => {
      dispatch(setEditingTransaction({}))
      history.push('/edit-transaction')
    },
    forceShowNav: () => dispatch(setShowNav(true)),
    createOrEditTransaction: (transaction: Partial<ITransaction>) => dispatch(postTransaction(transaction)),
    deleteTransaction: (id: string) => dispatch(removeTransaction(id)),
    openCategory: (category: string) => dispatch(push(category))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsContainer)
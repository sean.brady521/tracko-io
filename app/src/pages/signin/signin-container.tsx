import React, { useEffect } from 'react'
import Signin, { SigninProps } from './signin'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'
import { setShowNav } from 'store/user/user-actions'
import { signInWithPassword } from 'utils/auth'

interface SigninContainerProps extends SigninProps {
  setShowNav: (show: boolean) => void
}

export const SigninContainer = ({...props }: SigninContainerProps) => {
  const { setShowNav } = props

  useEffect(() => {
    setShowNav(false)
  }, [setShowNav])

  return (
    <Signin {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { user } = state
  return {
    loggedIn: user.loggedIn,
    error: user.error,
    onSubmit: signInWithPassword
  }
}

export function mapDispatchToProps(dispatch: any) {
  return { 
    setShowNav: (show: boolean) => dispatch(setShowNav(show)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SigninContainer)
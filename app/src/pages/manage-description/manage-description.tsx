import { IonIcon, IonInput } from '@ionic/react'
import PageWrapper from 'components/page-wrapper'
import { basket, checkmarkSharp, close } from 'ionicons/icons'
import React, { useState } from 'react'
import styles from './manage-description.module.scss'

const MAX_LETTERS = 50

export interface ManageDescriptionProps { 
  defaultDescription?: string
  backLink?: string
  onSave: (name: string) => void
}

const ManageDescription = (props: ManageDescriptionProps) => {
  const { backLink, defaultDescription, onSave } = props

  const [description, setDescription] = useState<string>(defaultDescription || '')

  const letterCountLeft = MAX_LETTERS - description.length

  return (
    <PageWrapper 
      title='Transaction description' 
      backLink={backLink}
      rightIcon={checkmarkSharp}
      leftIcon={close}
      onClickRightIcon={() => onSave(description)}
    >
      <div className={styles.contentWrap}>
        <div className={styles.descriptionNameWrap}>
          <IonInput 
            value={description} 
            autofocus
            placeholder='Type description name'
            maxlength={MAX_LETTERS}
            className={styles.nameInput} onIonChange={e => setDescription(e.detail.value!)}
            color='primary'
          />
        </div>
        <div className={styles.letterCount}>
          {letterCountLeft}
        </div>
      </div>
    </PageWrapper>
  ) 
}

export default ManageDescription
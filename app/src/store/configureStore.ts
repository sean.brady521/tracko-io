import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'

import userReducer from './user/user-reducers'
import transactionsReducer from './transactions/transactions-reducers'
import categoriesReducer from './categories/categories-reducers'
import { StoreState } from 'types/common'

export const history = createBrowserHistory()

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const initialState = {}

const configureStore = () => {
  const store = createStore(
    combineReducers<StoreState>({
      router: connectRouter(history),
      user: userReducer,
      transactions: transactionsReducer,
      categories: categoriesReducer,
    }),
    initialState,
    composeEnhancers(
      applyMiddleware(thunk, routerMiddleware(history))
    )
  )

  return store
}

export default configureStore

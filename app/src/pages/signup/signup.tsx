import { IonButton, IonHeader, IonInput, IonText } from '@ionic/react'
import PageWrapper from 'components/page-wrapper'
import { useEffect, useRef, useState } from 'react'
import { Redirect } from 'react-router'
import styles from './signup.module.scss'

export interface SignupProps {
  loggedIn: boolean
  handleSignUp: (email: string, password: string) => void
}

const Signup = (props: SignupProps) => {
  const { loggedIn ,handleSignUp } = props

  const [phase, setPhase] = useState<'email' | 'password'>('email')
  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  return (
    <PageWrapper 
      title=''
      backLink='/welcome'
    >
      <div className={styles.root}>
          <IonText color='dark'>
            <h1 className={styles.signUpText}>
              {phase === 'email' ? 'Sign up with email' : 'Create a password'}
            </h1>
          </IonText>
        <IonInput 
          color='primary' 
          autofocus
          type='email'
          value={phase === 'email' ? email : password}
          onIonChange={e => { 
            if (phase === 'email') {
              setEmail(e.detail.value!)
            } else {
              setPassword(e.detail.value!)
            }
          }}
          className={styles.emailInput} 
        />
        <div className={styles.buttonWrap}>
          <IonButton 
            color='primary' 
            shape='round' 
            size='large'
            className={styles.continue}
            onClick={() => {
              if (phase === 'email') {
                setPhase('password')
              } else {
                handleSignUp(email, password)
              }
            }}
          >
            Continue
          </IonButton>
        </div>
      </div>
      {loggedIn && <Redirect to='/categories' />}
    </PageWrapper>
  )
}

export default Signup
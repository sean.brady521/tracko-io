import { IonButton, IonIcon, IonPage, IonText } from '@ionic/react'
import { mailOutline, wallet } from 'ionicons/icons'
import React from 'react'
import styles from './welcome.module.scss'

interface WelcomeProps {}

const Welcome = (props: WelcomeProps) => {
  return (
    <IonPage>
      <div className={styles.contentWrap}>
        <div className={styles.headerWrap}>
          <IonIcon icon={wallet} color='primary' className={styles.headerIcon} />
          <IonText color='dark'>
            <div className={styles.header}>
              Tracko
            </div>
          </IonText>
        </div>
        <IonText color='medium'>
          <div className={styles.blurb}>
            Track your expenses by category, no fluff and no fuss.
          </div>
        </IonText>
        <IonButton 
          fill='clear' 
          shape='round' 
          className={styles.createButton} 
          routerLink={'/signup'} 
          routerDirection='none'
        >
          <div className={styles.createInner}>
            Create an account
          </div>
        </IonButton>
        <div className={styles.loginButtons}>
          <IonButton 
            color='primary' 
            shape='round' 
            className={styles.emailButton} 
            routerLink='/signin' 
            routerDirection='none'
          >
            <div className={styles.buttonInner}>
              <IonIcon icon={mailOutline} className={styles.mailIcon} />
              Sign in with Email
            </div>
          </IonButton>
        </div>
      </div>
    </IonPage>
  )
}

export default Welcome
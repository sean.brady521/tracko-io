import React from 'react'
import { IonIcon } from '@ionic/react'
import PageWrapper from 'components/page-wrapper'
import { help } from 'ionicons/icons'
import { ICategory } from 'types/common'
import styles from './manage-tr-category.module.scss'


export interface ManageTrCategoryProps { 
  defaultCategory?: Partial<ICategory>
  categories: ICategory[]
  backLink?: string
  onSelect: (cat: Partial<ICategory>) => void
}

const ManageTrCategory = (props: ManageTrCategoryProps) => {
  const { backLink, categories, onSelect } = props

  return (
    <PageWrapper 
      title='Transaction category' 
      backLink={backLink}
    >
      <div className={styles.contentWrap}>
        {categories.map(cat => (
          <div key={cat.id} className={styles.iconSelectWrap} onClick={() => onSelect(cat)}>
            <IonIcon icon={cat?.icon || help} className={styles.iconSelect} color='dark' />
            <div className={styles.catName}>
              {cat.name}
            </div>
          </div>
        ))}
      </div>
    </PageWrapper>
  ) 
}

export default ManageTrCategory
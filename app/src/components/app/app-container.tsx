import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { StoreState, } from 'types/common'
import App from './app'
import { initAuthManager } from 'store/user/user-actions'
import { initialiseFirebase } from 'utils/auth'
import { fetchTransactions } from 'store/transactions/transactions-actions'
import { fetchCategories } from 'store/categories/categories-actions'

interface AppContainerProps {
  loading: boolean
  startAuthManager: () => void
}

export const AppContainer = (props: AppContainerProps) => {
  const { startAuthManager, ...rest } = props

  useEffect(() => {
    initialiseFirebase()
    startAuthManager()
  }, [startAuthManager])

  return <App {...rest} />
}

export function mapStateToProps(state: StoreState) {
  const { loading: authenticating, error } = state.user
  const loginFailed = error === '401: login failed'

  const fetching = state.transactions.loading || 
    state.categories.loading ||
    !state.transactions.initialised || 
    !state.categories.initialised

  return { 
    loading: authenticating || (fetching && !loginFailed)
  }
}

export function mapDispatchToProps(dispatch: any) {
  const fetchData = () => {
    dispatch(fetchTransactions())
    dispatch(fetchCategories())
  }

  const startAuthManager = async () => {
    dispatch(initAuthManager(fetchData)) 
  }

  return {
    startAuthManager,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)
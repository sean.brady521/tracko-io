import { Route } from 'react-router-dom';
import { IonApp, IonLoading, IonRouterOutlet, useIonLoading } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import TrackPage from 'pages/track';
import TrackCategory from 'pages/track-category';
import ManageTransaction from 'pages/manage-transaction';
import ReportPage from 'pages/report';
import SettingsPage from 'pages/settings';
import SigninPage from 'pages/signin';
import SignupPage from 'pages/signup';
import NavBar from 'components/nav-bar';
import ManageCategory from 'pages/manage-category';
import ManageDescription from 'pages/manage-description';
import ManageTrCategory from 'pages/manage-tr-category';
import WelcomePage from 'pages/welcome';
import styles from './app.module.scss'

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import 'theme/ionic-variables.css';

/* global overrides */
import 'theme/global.scss'
import { useEffect } from 'react';

interface AppProps {
  loading: boolean
}

const App = (props: AppProps) => { 
  const { loading } = props

  const [present, dismiss] = useIonLoading()

  useEffect(() => {
    if (loading) { 
      present({ cssClass: styles.loader })
      // Additional backup dismiss for buggy shit
      setTimeout(dismiss, 3000);
    } else { 
      dismiss()
    }
  }, [loading, present, dismiss])


  return (
    <IonApp>
      <IonReactRouter>
        <NavBar>
          <IonRouterOutlet >
            <Route exact path="/" component={TrackPage} />
            <Route exact path="/welcome" component={WelcomePage} />
            <Route exact path="/signin" component={SigninPage} />
            <Route exact path="/signup" component={SignupPage} />
            <Route exact path="/categories" component={TrackPage} />
            <Route exact path="/category-transactions" component={TrackCategory} />
            <Route exact path="/edit-category" component={ManageCategory} />
            <Route exact path="/edit-description" component={ManageDescription} />
            <Route exact path="/edit-tr-category" component={ManageTrCategory} />
            <Route exact path="/edit-transaction" component={ManageTransaction} />
            <Route exact path="/report" component={ReportPage} />
            <Route exact path="/settings" component={SettingsPage} />
          </IonRouterOutlet>
        </NavBar>
      </IonReactRouter>
    </IonApp>
  )
}

export default App

import { ITransaction } from 'types/common'

export const REMOVE_TRANSACTION_BEGIN = 'REMOVE_TRANSACTION_BEGIN'
export const REMOVE_TRANSACTION_SUCCESS = 'REMOVE_TRANSACTION_SUCCESS'
export const REMOVE_TRANSACTION_FAILURE = 'REMOVE_TRANSACTION_FAILURE'
export const POST_TRANSACTION_BEGIN = 'POST_TRANSACTION_BEGIN'
export const POST_TRANSACTION_SUCCESS = 'POST_TRANSACTION_SUCCESS'
export const POST_TRANSACTION_FAILURE = 'POST_TRANSACTION_FAILURE'
export const FETCH_TRANSACTIONS_BEGIN = 'FETCH_TRANSACTIONS_BEGIN'
export const FETCH_TRANSACTIONS_SUCCESS = 'FETCH_TRANSACTIONS_SUCCESS'
export const FETCH_TRANSACTIONS_FAILURE = 'FETCH_TRANSACTIONS_FAILURE'
export const SET_EDITING_TRANSACTION = 'SET_EDITING_TRANSACTION'
export const UPDATE_EDITING_TRANSACTION = 'UPDATE_EDITING_TRANSACTION'

interface ActionFail {
  type: typeof REMOVE_TRANSACTION_FAILURE | typeof POST_TRANSACTION_FAILURE | typeof FETCH_TRANSACTIONS_FAILURE
  payload: {
    error: string
  }
}

interface SetEditingTransactionAction {
  type: typeof SET_EDITING_TRANSACTION | typeof UPDATE_EDITING_TRANSACTION
  payload: {
    editingTransaction: Partial<ITransaction>
  }
}

interface ActionBeginAsync {
  type: typeof FETCH_TRANSACTIONS_BEGIN
}

interface ActionSuccessImmediate {
  type: typeof REMOVE_TRANSACTION_SUCCESS | typeof POST_TRANSACTION_SUCCESS
}

interface PostTransactionBegin {
  type: typeof POST_TRANSACTION_BEGIN
  payload: {
    transaction: ITransaction
  }
}

interface RemoveTransactionBegin {
  type: typeof REMOVE_TRANSACTION_BEGIN
  payload: {
    id: string
  }
}

interface GetTransactionsAction {
  type: typeof FETCH_TRANSACTIONS_SUCCESS
  payload: {
    transactions: ITransaction[]
  }
}

export type TransactionActionTypes = 
  ActionFail |
  ActionBeginAsync |
  ActionSuccessImmediate |
  PostTransactionBegin |
  RemoveTransactionBegin |
  GetTransactionsAction |
  SetEditingTransactionAction

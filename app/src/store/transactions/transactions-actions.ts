import { fetchAll, addOrModifyEntry, removeById } from 'utils/server'
import { Dispatch } from 'redux'
import { StoreState, ITransaction, IEditingTransaction } from 'types/common'

const _removeTransactionBegin = (id: string) => ({
  type: 'REMOVE_TRANSACTION_BEGIN',
  payload: { id }
})

const _removeTransactionSuccess = () => ({
  type: 'REMOVE_TRANSACTION_SUCCESS',
})

const _removeTransactionFailure = (error: Error) => ({
  type: 'REMOVE_TRANSACTION_FAILURE',
  payload: { error }
})

const _postTransactionBegin = (transaction: Partial<ITransaction>) => ({
  type: 'POST_TRANSACTION_BEGIN',
  payload: { transaction }
})

const _postTransactionSuccess = () => ({
  type: 'POST_TRANSACTION_SUCCESS',
})

const _postTransactionFailure = (error: Error) => ({
  type: 'POST_TRANSACTION_FAILURE',
  payload: { error }
})

const _fetchTransactionsBegin = () => ({
  type: 'FETCH_TRANSACTIONS_BEGIN',
})

const _fetchTransactionsSuccess = (transactions: ITransaction[]) => ({
  type: 'FETCH_TRANSACTIONS_SUCCESS',
  payload: { transactions }
})

const _fetchTransactionsFailure = (error: Error) => ({
  type: 'FETCH_TRANSACTIONS_FAILURE',
  payload: { error }
})

export const setEditingTransaction = (transaction: Partial<IEditingTransaction>) => ({
  type: 'SET_EDITING_TRANSACTION',
  payload: { editingTransaction: transaction }
})

export const updateEditingTransaction = (transaction: Partial<IEditingTransaction>) => ({
  type: 'UPDATE_EDITING_TRANSACTION',
  payload: { editingTransaction: transaction }
})

export const removeTransaction = (id: string) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { user } = getState()

    if (!user.loggedIn) {
      dispatch(_removeTransactionSuccess())
      return
    }

    const onSuccess = () => {
      dispatch(_removeTransactionSuccess())
    }
    const onFailure = (err: any) => dispatch(_removeTransactionFailure(err))
    dispatch(_removeTransactionBegin(id))
    return removeById('transaction', id, onSuccess, onFailure)
  }
}

export const postTransaction = (transaction: Partial<ITransaction>) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { user } = getState()

    if (!user.loggedIn) { 
      dispatch(_postTransactionSuccess())
      return
    }

    const onSuccess = () => { 
      dispatch(_postTransactionSuccess())
    }
    const onFailure = (err: any) => dispatch(_postTransactionFailure(err))
    dispatch(_postTransactionBegin(transaction))
    return addOrModifyEntry('transaction', transaction, onSuccess, onFailure)
  }
}

export const fetchTransactions = () => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { transactions, user } = getState()

    if (!user.loggedIn) return

    if (transactions.transactions.length ||transactions.loading) return // Weve already fetched

    const onSuccess = (transactions: ITransaction[]) => { 
      dispatch(_fetchTransactionsSuccess(transactions))
      window.localStorage.setItem('transaction_count', `${transactions.length}`)
    }
    const onFailure = (transactions: any) => dispatch(_fetchTransactionsFailure(transactions))
    dispatch(_fetchTransactionsBegin())
    return fetchAll('transactions', onSuccess, onFailure)
  }
}

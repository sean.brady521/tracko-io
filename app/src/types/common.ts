import { RouterState } from 'connected-react-router'

export interface ITransaction {
  id: string
  createdBy: string
  timestamp: number
  amount: number
  description?: string
  categoryId?: string
}

export interface EditTransaction extends Omit<ITransaction, 'id' | 'createdBy'> {
  category?: Partial<ICategory>
  id?: string
}

export interface ICategory {
  id: string
  name: string
  icon: string
  createdBy: string
  timestamp: number
}


export interface StoreState {
  router: RouterState
  user: UserState
  transactions: TransactionsState
  categories: CategoriesState
}

// export interface RouterState {
//   location: Location
//   action: string
// }

export interface UserState {
  loading: boolean
  loggedIn: boolean
  showNav: boolean
  error: string | null
}

export interface IEditingTransaction extends Partial<ITransaction> {
  category?: Partial<ICategory>
}

export interface TransactionsState {
  transactions: ITransaction[]
  editingTransaction: IEditingTransaction
  error: string | null
  loading: boolean
  initialised: boolean
}

export interface CategoriesState {
  categories: ICategory[]
  editingCategory: Partial<ICategory>
  error: string | null
  loading: boolean
  initialised: boolean
}

export interface Location {
  pathname: string
  search: string
  hash: string
}

export interface TransactionsByDate {
  [ts: string]: ITransaction[]
}

import { IonButton, IonPage } from '@ionic/react'
import React from 'react'
import { signOut } from 'utils/auth'
import styles from './settings.module.scss'

interface SettingsProps {}

const Settings: React.FC = (props: SettingsProps) => {
  return (
    <IonPage>
      <div className={styles.root}>
        <IonButton onClick={() => signOut()}>
          sign out
        </IonButton>
      </div>
    </IonPage>
  ) 
}

export default Settings
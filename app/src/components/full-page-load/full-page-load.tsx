import React from 'react'
import styles from './full-page-load.module.scss'

const FullPageLoad = () => {
  return (
    <div className={styles.root}>
      BEANS
    </div>
  )
}

export default FullPageLoad

import React from 'react'
import moment from 'moment'
import { IonFab, IonFabButton, IonIcon } from '@ionic/react'
import PageWrapper from 'components/page-wrapper'
import { add, search } from 'ionicons/icons'
import { ICategory, ITransaction, TransactionsByDate } from 'types/common'
import styles from './track-category.module.scss'
import TransactionItem from './transaction-item'

const dummyData = {
  '29 JAN': [
    { id: '1', description: 'Cafe + Beans', amount: 18.00, timestamp: 1611878400 },
    { id: '2', description: 'Eggs', amount: 2.50, timestamp: 1612224000 },
  ],
  '15 JAN': [
    { id: '3', description: 'Bread', amount: 7.50, timestamp: 1609632000 },
  ],
  '2 JAN': [
    { id: '4', description: 'Body Wash', amount: 26.00, timestamp: 1609459200 },
  ]
}

export interface TrackCategoryProps {
  category: ICategory
  transactionsByDate: TransactionsByDate 
  onAddTransaction: () => void
  onEditTransaction: (transaction: Pick<ITransaction, 'amount' | 'description' | 'id' | 'timestamp'>) => void
}

const TrackCategory = (props: TrackCategoryProps) => {
  const { category, transactionsByDate = {}, onAddTransaction, onEditTransaction } = props
  console.log(transactionsByDate)

  return (
    <PageWrapper 
      title={category.name} 
      backLink='/categories' 
      rightIcon={search}
      rightIconColour='medium'
      onClickRightIcon={() => {
        console.log('search')
      }}
    >
      <div className={styles.contentWrap}>
        <div className={styles.transactions}>
          {Object.entries(transactionsByDate).map(([date, transactions]) => (
            <div key={date}>
              <div className={styles.dateWrap}>
                <div className={styles.date}>
                  {moment(parseInt(date)).format('D MMM')}
                </div>
              </div>
              <div className={styles.tranGroup}>
                {transactions.map(tran => (
                  <TransactionItem
                    onClick={() => onEditTransaction(tran)}
                    key={tran.id}
                    description={tran.description}
                    amount={tran.amount}
                    className={styles.item}
                  />
                ))}
              </div>
            </div>
          ))}
        </div>
      </div>
      <IonFab
        horizontal='end'
        vertical='bottom'
      >
        <IonFabButton onClick={onAddTransaction} routerDirection='none'>
          <IonIcon icon={add} />
        </IonFabButton>
      </IonFab>
    </PageWrapper>
  )
}

export default TrackCategory
import { ICategory } from 'types/common'

export const REMOVE_CATEGORY_BEGIN = 'REMOVE_CATEGORY_BEGIN'
export const REMOVE_CATEGORY_SUCCESS = 'REMOVE_CATEGORY_SUCCESS'
export const REMOVE_CATEGORY_FAILURE = 'REMOVE_CATEGORY_FAILURE'
export const POST_CATEGORY_BEGIN = 'POST_CATEGORY_BEGIN'
export const POST_CATEGORY_SUCCESS = 'POST_CATEGORY_SUCCESS'
export const POST_CATEGORY_FAILURE = 'POST_CATEGORY_FAILURE'
export const FETCH_CATEGORIES_BEGIN = 'FETCH_CATEGORIES_BEGIN'
export const FETCH_CATEGORIES_SUCCESS = 'FETCH_CATEGORIES_SUCCESS'
export const FETCH_CATEGORIES_FAILURE = 'FETCH_CATEGORIES_FAILURE'
export const SET_EDITING_CATEGORY = 'SET_EDITING_CATEGORY'

interface ActionFail {
  type: typeof REMOVE_CATEGORY_FAILURE | typeof POST_CATEGORY_FAILURE | typeof FETCH_CATEGORIES_FAILURE
  payload: {
    error: string
  }
}

interface ActionBeginAsync {
  type: typeof FETCH_CATEGORIES_BEGIN
}

interface ActionSuccessImmediate {
  type: typeof REMOVE_CATEGORY_SUCCESS | typeof POST_CATEGORY_SUCCESS
}

interface PostCategoryBegin {
  type: typeof POST_CATEGORY_BEGIN
  payload: {
    category: ICategory
  }
}

interface RemoveCategoryBegin {
  type: typeof REMOVE_CATEGORY_BEGIN
  payload: {
    id: string
  }
}

interface GetCategoriesAction {
  type: typeof FETCH_CATEGORIES_SUCCESS
  payload: {
    categories: ICategory[]
  }
}

interface SetEditingCategoryAction {
  type: typeof SET_EDITING_CATEGORY
  payload: {
    editingCategory: Partial<ICategory>
  }
}



export type CategoryActionTypes = 
  ActionFail |
  ActionBeginAsync |
  ActionSuccessImmediate |
  PostCategoryBegin |
  RemoveCategoryBegin |
  GetCategoriesAction |
  SetEditingCategoryAction
import { IonButton, IonDatetime, IonIcon, IonText } from '@ionic/react'
import { checkmarkSharp } from 'ionicons/icons'
import React, { useState } from 'react'
import { EditTransaction, IEditingTransaction, ITransaction } from 'types/common'
import styles from './manage-transaction.module.scss'
import CurrencyInput from 'react-currency-input-field'
import PageWrapper from 'components/page-wrapper'
import moment from 'moment'
import classNames from 'classnames'
import { getUrlWithParams } from 'utils/url'
import { Redirect } from 'react-router'

export interface ManageTransactionProps { 
  type: 'Add' | 'Edit'
  backLink: string
  editingTransaction: IEditingTransaction
  onSave: (transaction: EditTransaction) => void
}

const ManageTransaction = (props: ManageTransactionProps) => {
  const { type, editingTransaction, backLink, onSave } = props

  const [amount, setAmount] = useState<string>(`${editingTransaction.amount || '0.00'}`)
  const [timestamp, setTimestamp] = useState<number>(editingTransaction.timestamp || Date.now())
  const [redirectTo, setRedirectTo] = useState<string | null>(null)


  const editDescUrl = getUrlWithParams('edit-description', { tranId: editingTransaction.id })
  const editCatUrl = getUrlWithParams('edit-tr-category', { tranId: editingTransaction.id })

  const { description, category } = editingTransaction

  const addOrEditButton = (text: 'Edit' | 'Add' | React.ReactNode, editUrl :string) => (
    <IonButton 
      fill='clear' 
      routerLink={editUrl}
      routerDirection='none'
      className={styles.sectionButton}
    >
      {text}
    </IonButton>
  )

  return (
    <PageWrapper 
      title={`${type} transaction`} 
      backLink={backLink}
      rightIcon={checkmarkSharp}
      onClickRightIcon={() => {
        const newTransaction: EditTransaction = {
          ...editingTransaction,
          amount: parseFloat(amount.replace('$', '')),
          timestamp
        }
        onSave(newTransaction)
        setRedirectTo('/categories')
      }}
    >
      <div className={styles.contentWrap}>
        <div className={styles.amountWrap}>
          <div className={styles.amountText}>
            <IonText color='dark'>
              Amount
            </IonText>
          </div>
          <CurrencyInput 
            className={styles.amountInput}
            defaultValue={0}
            onFocus={() => { 
              setAmount(amount.replace(/\.0+$/, ''))
            }}
            onBlur={() => { 
              setAmount(parseFloat(amount).toFixed(2))
            }}
            prefix='$'
            value={amount}
            onValueChange={v => setAmount(v || '0') }
          />
        </div>
        <div className={styles.inputSection}>
          <div className={styles.descWrap}>
            <div className={styles.descMainRow}>
              <IonText color='dark'>
                Description
              </IonText>
              <IonText color='medium'>
                {!!description && description}
              </IonText>
              {!description && addOrEditButton('Add', editDescUrl)}
            </div>
            {!!description && 
              <div className={styles.descSubRow}>
                {addOrEditButton('Edit', editDescUrl)}
              </div>
            }
          </div>
        </div>
        <div className={styles.inputSection}>
          <IonText color='dark'>
            Category
          </IonText>
          <div className={styles.sectionValueWrap}>
            {!!category && 
              addOrEditButton(
                (
                  <div className={styles.categoryValue}>
                    <IonIcon icon={category.icon} className={styles.categoryIcon} />
                    {category.name}
                  </div>
                ), 
                editCatUrl
              )
            }
            {!category && addOrEditButton('Add', editCatUrl)}
          </div>
        </div>
        <div className={styles.inputSection}>
          <IonText color='dark'>
            Date
          </IonText>
          <div className={styles.sectionValueWrap}>
            <IonDatetime 
              value={moment(timestamp).format()} 
              className={classNames(styles.sectionButton, styles.datetime)} 
              displayFormat='DDD, D MMM YYYY'
              max={`${moment().year() + 4}`}
              onIonChange={e => setTimestamp(moment(e.detail.value).valueOf())}
            />
          </div>
        </div>
      </div>
      {redirectTo && <Redirect to={redirectTo} />}
    </PageWrapper>
  ) 
}

export default ManageTransaction
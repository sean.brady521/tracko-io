import React from 'react'
import styles from './transaction-item.module.scss'
import classNames from 'classnames'
import { IonText } from '@ionic/react'

interface TransactionItemProps {
  amount: number
  onClick: () => void
  description?: string
  className?: string
}

const TransactionItem = (props: TransactionItemProps) => {
  const { amount, description, className, onClick } = props
  return (
    <div className={classNames(styles.root, className)} onClick={onClick}>
      <IonText color='medium'>
        {description}
      </IonText>
      <div className={styles.amount}>
        <IonText color='medium'>
          {amount}
        </IonText>
      </div>
    </div>
  )
}

export default TransactionItem
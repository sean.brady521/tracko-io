import { IonCard, IonIcon } from '@ionic/react'
import React from 'react'
import styles from './category-card.module.scss'
import { basket, } from 'ionicons/icons'

interface CategoryCardProps { 
  icon?: string
  name: string
  amount: number
  className?: string
  routerLink?: string
  onClick?: () => void
}

const CategoryCard = (props: CategoryCardProps) => {
  const { icon = basket, name, amount, className, routerLink, onClick } = props

  return (
    <IonCard onClick={onClick} button routerLink={routerLink} routerDirection='none' className={className}>
      <div className={styles.root}>
        <div className={styles.iconAndName}>
          <IonIcon icon={icon} className={styles.icon} />
          <div className={styles.name}>
            {name}
          </div>
        </div>
        <div className={styles.amount}>
          ${amount}
        </div>
      </div>
    </IonCard>
  )
}

export default CategoryCard
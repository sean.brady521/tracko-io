export const SET_LOGGED_IN = 'SET_LOGGED_IN'
export const SET_LOADING = 'SET_LOADING'
export const SET_ERROR = 'SET_ERROR'
export const SET_SHOW_NAV = 'SET_SHOW_NAV'
export const LOGOUT = 'LOGOUT'

interface SetLoggedInAction {
  type: typeof SET_LOGGED_IN
  payload: {
    loggedIn: boolean
  }
}

interface SetLoadingAction {
  type: typeof SET_LOADING
  payload: {
    loading: boolean
  }
}

interface SetShowNavAction {
  type: typeof SET_SHOW_NAV
  payload: {
    showNav: boolean
  }
}

interface SetErrorAction {
  type: typeof SET_ERROR
  payload: {
    error: string
  }
}

export type UserActionTypes = SetLoggedInAction | SetLoadingAction | SetErrorAction | SetShowNavAction
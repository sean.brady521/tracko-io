const functions = require('firebase-functions')

import express from 'express'

import admin from 'firebase-admin'

import cors from 'cors'

import TransactionRoutes from './routes/transaction-routes'
import CategoryRoutes from './routes/category-routes'

const app = express()

app.use(cors({ origin: true }))
app.use(express.json())
app.use(express.urlencoded())

admin.initializeApp()

TransactionRoutes(app)
CategoryRoutes(app)

exports.app = functions.https.onRequest(app)
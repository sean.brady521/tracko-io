import admin from 'firebase-admin'
import { Response, Express } from 'express'
import { Category, VerifiedRequest } from '../types/common'
import verifyToken from '../auth/VerifyTokens' 

export default (app: Express) => {
  app.get('/api/categories', verifyToken, async (req: VerifiedRequest, res: Response) => {
    const db = admin.firestore()
    const categoryMatches = await db.collection('categories').where('createdBy', '==', req.userId).get()

    const categorys: Category[] = []
    if (!categoryMatches.empty) {
      categoryMatches.forEach(category => { categorys.push(category.data() as Category) })
    }

    res.status(200).send(categorys)
  })

  app.post('/api/category', verifyToken, async (req: VerifiedRequest, res: Response) => {
    const db = admin.firestore()

    console.log(`category with id ${req.body.id}`)
    const docRef = db.collection('categories').doc(req.body.id)
    const existingTran = await docRef.get()

    // Make sure we're not overwriting someone elses Category
    if (existingTran.exists) {
      const existingTranData = existingTran.data() as Category
      if (existingTranData.createdBy !== req.userId) {
        res.status(403).send('Cant go editing another guys Category, bit rude')
        return
      }
    }

    await docRef.set(req.body)

    res.status(200).send({ id: req.body.id })
  })

  app.delete('/api/category', verifyToken, async (req: VerifiedRequest, res: Response) => {
    const { userId, body } = req

    const db = admin.firestore()
    const docRef = db.collection('categories').doc(body.id)

    const category = await docRef.get()
    // Make sure this user has permission to delete this Category
    if (category.exists) {
      const existingTran = category.data() as Category
      if (existingTran.createdBy !== userId) {
        res.status(403).send('That aint ya Category')
      } else {
        await docRef.delete()
        res.status(200).send({ id: body.id })
      }
    } else {
      res.status(404).send('Category does not exist')
    }
  })
}

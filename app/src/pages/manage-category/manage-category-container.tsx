import React, { useEffect } from 'react'
import ManageCategory, { ManageCategoryProps } from './manage-category'
import { connect } from 'react-redux'
import { StoreState, ICategory } from 'types/common'
import { setShowNav } from 'store/user/user-actions'
import { postCategory } from 'store/categories/categories-actions'
import parseLocation from 'utils/parse-location'
import { getUserId } from 'utils/auth'
const { v4: uuidv4 } = require('uuid')

interface CategoriesContainerProps extends ManageCategoryProps { 
  setShowNav: (show: boolean) => void
}

export const ManageCategoryContainer = ({...props }: CategoriesContainerProps) => {
  const { setShowNav } = props

  useEffect(() => {
    setShowNav(false)
  }, [setShowNav])

  return (
    <ManageCategory {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { categories } = state.categories

  const { categoryId } = parseLocation(window.location)

  const type: 'Add' | 'Edit' = categoryId ? 'Edit' : 'Add'

  const matchingCat: Partial<ICategory> = categories.find(c => c.id === categoryId) || {}

  return {
    defaultCategory: matchingCat.name,
    type,
    onSave: (name: string) => console.log(name)
  }
}

export function mapDispatchToProps(dispatch: any) {
  const { categoryId } = parseLocation(window.location)

  return { 
    setShowNav: (show: boolean) => dispatch(setShowNav(show)),
    onSave: (name: string, icon: string) => {
      const userId = getUserId()
      if (!userId) return

      const category: ICategory = {
        id:  categoryId ?? uuidv4(),
        createdBy: userId,
        timestamp: Date.now(),
        name,
        icon
      }
      dispatch(postCategory(category))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageCategoryContainer)
export function getUrlWithParams(url: string, params: { [key: string]: string | undefined }): string {
  let isFirstParam = false
  let urlWithParams = `/${url}`
  for (const [key, value] of Object.entries(params)) {
    if (!value) continue
    if (isFirstParam) {
      urlWithParams += `?${key}=${value}`
    } else {
      urlWithParams += `&${key}=${value}`
    }
  }
  return urlWithParams
}
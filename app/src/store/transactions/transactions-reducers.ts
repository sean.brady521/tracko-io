import { ITransaction, TransactionsState } from 'types/common'
import {
  REMOVE_TRANSACTION_BEGIN,
  REMOVE_TRANSACTION_SUCCESS,
  REMOVE_TRANSACTION_FAILURE,
  POST_TRANSACTION_BEGIN,
  POST_TRANSACTION_SUCCESS,
  POST_TRANSACTION_FAILURE,
  FETCH_TRANSACTIONS_BEGIN,
  FETCH_TRANSACTIONS_SUCCESS,
  FETCH_TRANSACTIONS_FAILURE,
  SET_EDITING_TRANSACTION,
  UPDATE_EDITING_TRANSACTION,
  TransactionActionTypes
} from './transactions-action-types'

const initialState: TransactionsState = {
  transactions: [],
  editingTransaction: {},
  loading: false,
  error: null,
  initialised: false,
}

function updateOrAdd(currTransactions: ITransaction[], incTransaction: ITransaction) {
  const newTransactions = [...currTransactions]

  let found = false
  for (let i = 0 ; i < newTransactions.length ; i++) {
    if (newTransactions[i].id === incTransaction.id) {
      newTransactions[i] = incTransaction
      found = true
      break
    }
  }
  if (!found) newTransactions.push(incTransaction)
  return newTransactions
}

const transactionReducers = (
  state = initialState,
  action: TransactionActionTypes
  ): TransactionsState => {
  switch (action.type) {
    case REMOVE_TRANSACTION_BEGIN:
      return {
        ...state,
        error: null,
        transactions: state.transactions.filter(transaction => transaction.id !== action.payload.id)
      }
    case REMOVE_TRANSACTION_SUCCESS:
      return {
        ...state
      }
    case REMOVE_TRANSACTION_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      }
    case POST_TRANSACTION_BEGIN:
      return {
        ...state,
        error: null,
        transactions: updateOrAdd(state.transactions, action.payload.transaction)
      }
    case POST_TRANSACTION_SUCCESS:
      return {
        ...state,
      }
    case POST_TRANSACTION_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      }
    case FETCH_TRANSACTIONS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_TRANSACTIONS_SUCCESS:
      return {
        ...state,
        loading: false,
        initialised: true,
        transactions: action.payload.transactions
      }
    case FETCH_TRANSACTIONS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        initialised: true,
        transactions: []
      }
    case SET_EDITING_TRANSACTION:
      return {
        ...state,
        editingTransaction: action.payload.editingTransaction
      }
    case UPDATE_EDITING_TRANSACTION:
      return {
        ...state,
        editingTransaction: { 
          ...state.editingTransaction,
          ...action.payload.editingTransaction
        }
      }
    default:
      return state
  }
}

export default transactionReducers
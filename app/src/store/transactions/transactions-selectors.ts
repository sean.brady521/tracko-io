import { ITransaction, TransactionsByDate } from "types/common";

export function selectFilteredTransactions (transactions: ITransaction[], search: string): ITransaction[] {
  return transactions.filter(t => t.description && t.description.includes(search))
}

export function selectCategoryTransactions (transactions: ITransaction[], categoryId: string): ITransaction[] {
  return transactions.filter(t => t.categoryId === categoryId)
}

export function selectTransactionsByDate(transactions: ITransaction[]): TransactionsByDate {
  const byDate: TransactionsByDate  = {}
  for (const tran of transactions) {
    if (!byDate[tran.timestamp]) byDate[tran.timestamp] = []
    byDate[tran.timestamp].push(tran)
  }
  return byDate
}

type Totals = { [category: string]: number }
export function selectTotalsByCategory(transactions: ITransaction[]): Totals {
  const totals: Totals = {}
  for (const tran of transactions) {
    if (!tran.categoryId) continue
    if (!totals[tran.categoryId]) totals[tran.categoryId] = 0

    totals[tran.categoryId] += tran.amount
  }
  return totals
}
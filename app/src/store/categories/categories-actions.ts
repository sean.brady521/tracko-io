import { fetchAll, addOrModifyEntry, removeById } from 'utils/server'
import { Dispatch } from 'redux'
import { StoreState, ICategory } from 'types/common'

const _removeCategoryBegin = () => ({
  type: 'REMOVE_CATEGORY_BEGIN',
})

const _removeCategorySuccess = (id: string) => ({
  type: 'REMOVE_CATEGORY_SUCCESS',
  payload: { id }
})

const _removeCategoryFailure = (error: Error) => ({
  type: 'REMOVE_CATEGORY_FAILURE',
  payload: { error }
})

const _postCategorySuccess = () => ({
  type: 'POST_CATEGORY_SUCCESS',
})

const _postCategoryBegin = (category: Partial<ICategory>) => ({
  type: 'POST_CATEGORY_BEGIN',
  payload: { category }
})

const _postCategoryFailure = (error: Error) => ({
  type: 'POST_CATEGORY_FAILURE',
  payload: { error }
})

const _fetchCategoriesBegin = () => ({
  type: 'FETCH_CATEGORIES_BEGIN',
})

const _fetchCategoriesSuccess = (categories: ICategory[]) => ({
  type: 'FETCH_CATEGORIES_SUCCESS',
  payload: { categories }
})

const _fetchCategoriesFailure = (error: Error) => ({
  type: 'FETCH_CATEGORIES_FAILURE',
  payload: { error }
})

export const setEditingCategory = (category: Partial<ICategory>) => ({
  type: 'SET_EDITING_CATEGORY',
  payload: { editingCategory: category }
})

export const removeCategory = (id: string) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { user } = getState()

    if (!user.loggedIn) {
      dispatch(_removeCategorySuccess(id))
      return
    }

    const onSuccess = () => {
      dispatch(_removeCategorySuccess(id))
    }
    const onFailure = (err: any) => dispatch(_removeCategoryFailure(err))
    dispatch(_removeCategoryBegin())
    return removeById('category', id, onSuccess, onFailure)
  }
}

export const postCategory = (category: Partial<ICategory>) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { user } = getState()

    if (!user.loggedIn) { 
      dispatch(_postCategorySuccess())
      return
    }

    const onSuccess = () => { 
      dispatch(_postCategorySuccess())
    }
    const onFailure = (err: any) => dispatch(_postCategoryFailure(err))
    dispatch(_postCategoryBegin(category))
    return addOrModifyEntry('category', category, onSuccess, onFailure)
  }
}

export const fetchCategories = () => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { categories, user } = getState()

    if (!user.loggedIn) return

    if (categories.categories.length ||categories.loading) return // Weve already fetched

    const onSuccess = (categories: ICategory[]) => { 
      dispatch(_fetchCategoriesSuccess(categories))
      window.localStorage.setItem('category_count', `${categories.length}`)
    }
    const onFailure = (categories: any) => dispatch(_fetchCategoriesFailure(categories))
    dispatch(_fetchCategoriesBegin())
    return fetchAll('categories', onSuccess, onFailure)
  }
}

import React from 'react';
import { IonTabs, IonTabBar, IonTabButton, IonIcon, IonLabel } from '@ionic/react';
import { walletOutline, pieChartOutline, cogOutline } from 'ionicons/icons';

export interface NavBarProps {
  children: React.ReactNode
  shouldHide: boolean
}

const NavBar = (props: NavBarProps) => { 
  const { children, shouldHide } = props

  if (shouldHide) return <div>{children}</div>
  
  return (
    <IonTabs>
      {children}
      <IonTabBar slot="bottom">
        <IonTabButton tab="track" href="/categories">
          <IonIcon icon={walletOutline} />
          <IonLabel>Track</IonLabel>
        </IonTabButton>

        <IonTabButton tab="report" href="/report">
          <IonIcon icon={pieChartOutline} />
          <IonLabel>Report</IonLabel>
        </IonTabButton>

        <IonTabButton tab="settings" href="/settings">
          <IonIcon icon={cogOutline} />
          <IonLabel>Settings</IonLabel>
        </IonTabButton>
      </IonTabBar>
    </IonTabs>
  )
}

export default NavBar
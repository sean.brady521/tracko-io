import React, { useEffect } from 'react'
import TrackCategory, { TrackCategoryProps } from './track-category'
import { connect } from 'react-redux'
import { StoreState, ITransaction, ICategory } from 'types/common'
import { RouteComponentProps } from 'react-router'
import parseLocation from 'utils/parse-location'
import { selectCategoryTransactions, selectTransactionsByDate } from 'store/transactions/transactions-selectors'
import { updateEditingTransaction } from 'store/transactions/transactions-actions'

interface TransactionsContainerProps extends TrackCategoryProps, RouteComponentProps { }

export const TransactionsContainer = ({...props }: TransactionsContainerProps) => {

  return (
    <TrackCategory {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { loggedIn } = state.user
  const { transactions, loading, error } = state.transactions
  const { categories } = state.categories

  const { categoryId } = parseLocation(window.location)
  const category = categories.find(c => c.id === categoryId) || { id: 'none', name: 'N/A', icon: '', createdBy: '', timestamp: 0 }

  return {
    transactionsByDate: selectTransactionsByDate(selectCategoryTransactions(transactions, categoryId)),
    category,
    loading,
    loggedIn,
    authenticating: state.user.loading,
    error,
  }
}

export function mapDispatchToProps(dispatch: any, ownProps: TransactionsContainerProps) {
  const { categoryId } = parseLocation(window.location)
  const { history } = ownProps
  return { 
    onAddTransaction: () => {
      dispatch(updateEditingTransaction({ categoryId }))
      history.push(`/edit-transaction?from=${categoryId}`)
    },
    onEditTransaction: (transaction: Pick<ITransaction, 'amount' | 'description' | 'id' | 'timestamp'>) => {
      dispatch(updateEditingTransaction(transaction))
      history.push(`/edit-transaction?tranId=${transaction.id}&from=${categoryId}`)
    }
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(TransactionsContainer)
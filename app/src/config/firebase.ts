export const firebaseConf = {
  apiKey: "AIzaSyCjVGQj4Rb0p4UJUH84UUBYz8yHHhHJR0M",
  authDomain: "tracko-6281c.firebaseapp.com",
  projectId: "tracko-6281c",
  storageBucket: "tracko-6281c.appspot.com",
  messagingSenderId: "361049036201",
  appId: "1:361049036201:web:7defa091786ad86de3e0b8",
  measurementId: "G-SHY8GQEYSC"
}

export const actionCodeSettings = {
  url: process.env.NODE_ENV === 'development' 
    ? 'http://localhost:8100' 
    : 'https://tracko.io',
  handleCodeInApp: true,
}

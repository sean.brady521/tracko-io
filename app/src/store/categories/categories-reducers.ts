import { ICategory, CategoriesState } from 'types/common'
import {
  REMOVE_CATEGORY_BEGIN,
  REMOVE_CATEGORY_SUCCESS,
  REMOVE_CATEGORY_FAILURE,
  POST_CATEGORY_BEGIN,
  POST_CATEGORY_SUCCESS,
  POST_CATEGORY_FAILURE,
  FETCH_CATEGORIES_BEGIN,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_CATEGORIES_FAILURE,
  SET_EDITING_CATEGORY,
  CategoryActionTypes
} from './categories-action-types'

const initialState: CategoriesState = {
  categories: [],
  editingCategory: {},
  loading: false,
  initialised: false,
  error: null,
}

function updateOrAdd(currCategories: ICategory[], incCategory: ICategory) {
  const newCategories = [...currCategories]

  let found = false
  for (let i = 0 ; i < newCategories.length ; i++) {
    if (newCategories[i].id === incCategory.id) {
      newCategories[i] = incCategory
      found = true
      break
    }
  }
  if (!found) newCategories.push(incCategory)
  return newCategories
}

const categoryReducers = (
  state = initialState,
  action: CategoryActionTypes
  ): CategoriesState => {
  switch (action.type) {
    case REMOVE_CATEGORY_BEGIN:
      return {
        ...state,
        categories: state.categories.filter(category => category.id !== action.payload.id),
        error: null
      }
    case REMOVE_CATEGORY_SUCCESS:
      return {
        ...state,
      }
    case REMOVE_CATEGORY_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      }
    case POST_CATEGORY_BEGIN:
      return {
        ...state,
        error: null,
        categories: updateOrAdd(state.categories, action.payload.category)
      }
    case POST_CATEGORY_SUCCESS:
      return {
        ...state,
      }
    case POST_CATEGORY_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      }
    case FETCH_CATEGORIES_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        loading: false,
        categories: action.payload.categories,
        initialised: true
      }
    case FETCH_CATEGORIES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        initialised: true,
        categories: []
      }
    case SET_EDITING_CATEGORY:
      return {
        ...state,
        editingCategory: action.payload.editingCategory
      }
    default:
      return state
  }
}

export default categoryReducers
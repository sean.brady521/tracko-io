import { IonPage } from '@ionic/react'
import React from 'react'
import styles from './report.module.scss'

interface ReportProps {}

const Report: React.FC = (props: ReportProps) => {
  return (
    <IonPage>
      <div className={styles.root}>
        report
      </div>
    </IonPage>
  ) 
}

export default Report
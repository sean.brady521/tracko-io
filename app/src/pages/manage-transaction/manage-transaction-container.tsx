import React, { useEffect } from 'react'
import ManageTransaction, { ManageTransactionProps } from './manage-transaction'
import { connect } from 'react-redux'
import { StoreState, EditTransaction, ITransaction } from 'types/common'
import { setShowNav } from 'store/user/user-actions'
import parseLocation from 'utils/parse-location'
import { postTransaction, setEditingTransaction } from 'store/transactions/transactions-actions'
import { getUserId } from 'utils/auth'
const { v4: uuidv4 } = require('uuid')

interface TransactionsContainerProps extends ManageTransactionProps { 
  setShowNav: (show: boolean) => void
}

export const ManageTransactionContainer = ({...props }: TransactionsContainerProps) => {
  const { setShowNav } = props

  useEffect(() => {
    setShowNav(false)
  }, [setShowNav])

  return (
    <ManageTransaction {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { loggedIn } = state.user
  const { transactions, loading, error, editingTransaction } = state.transactions
  const { categories } = state.categories

  const { tranId } = parseLocation(window.location)

  const editingTransactionCat = categories.find(cat => cat.id === editingTransaction.categoryId)

  const type: 'Add' | 'Edit' = tranId ? 'Edit' : 'Add'

  const { from } = parseLocation(window.location)

  const backLink = from ? `/category-transactions?categoryId=${from}` : '/categories'

  return {
    transactions,
    editingTransaction: {
      ...editingTransaction,
      category: editingTransactionCat
    },
    backLink,
    loading,
    loggedIn,
    type,
    authenticating: state.user.loading,
    error,
  }
}

export function mapDispatchToProps(dispatch: any) {
  return { 
    setShowNav: (show: boolean) => dispatch(setShowNav(show)),
    onSave: (transaction: EditTransaction) => {
      const userId = getUserId()
      if (!userId) return

      const { category, id, ...rest } = transaction
      const newTran: ITransaction = {
        ...rest,
        id: id || uuidv4(),
        categoryId: category?.id,
        createdBy: userId,
      }
      dispatch(postTransaction(newTran))
      dispatch(setEditingTransaction({}))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageTransactionContainer)
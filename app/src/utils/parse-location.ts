import queryString from 'query-string'
import { Location } from '../types/common'

export default function parseLocation (location: Location): any {
  const parsedSearch = queryString.parse(location.search)
  return {
    route: location.pathname.replace('/', '') || 'home',
    ...parsedSearch
  }
}

import { IonPage, IonButton, IonIcon, IonHeader, IonToolbar, IonText, IonBackButton } from '@ionic/react'
import { arrowBack } from 'ionicons/icons'
import React  from 'react'
import styles from './page-wrapper.module.scss'

export interface PageWrapperProps { 
  title: string
  rightIcon?: string
  rightIconColour?: string
  leftIcon?: string
  onClickRightIcon?: () => void
  backLink?: string
  children: React.ReactNode
}

const PageWrapper = (props: PageWrapperProps) => {

  const { 
    children, 
    title, 
    rightIcon, 
    rightIconColour = 'primary',
    leftIcon = arrowBack,
    backLink, 
    onClickRightIcon 
  } = props

  return (
    <IonPage className={styles.root}>
      <IonHeader>
        <IonToolbar>
          <div className={styles.toolbarInner}>
            <div className={styles.iconAndName}>
              <IonButton 
                fill='clear' 
                shape='round' 
                color='dark'
                className={styles.leftIconButton} 
                routerLink={backLink} 
                routerDirection='none'
              >
                <IonIcon icon={leftIcon} className={styles.backIcon} size='medium'/>
              </IonButton>

              <div className={styles.name}>
                <IonText color='dark'>
                  {title}
                </IonText>
              </div>
            </div>
            <IonButton 
              fill='clear' 
              shape='round' 
              color='medium' 
              className={styles.rightIconButton}
              onClick={onClickRightIcon}
            >
              <IonIcon icon={rightIcon} color={rightIconColour}/>
            </IonButton>
          </div>
        </IonToolbar>
      </IonHeader>
      {children}
    </IonPage>
  ) 
}

export default PageWrapper
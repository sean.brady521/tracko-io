import React, { useEffect } from 'react'
import Signup, { SignupProps } from './signup'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'
import { setShowNav } from 'store/user/user-actions'
import { signUpWithPassword } from 'utils/auth'

interface SignupContainerProps extends SignupProps {
  setShowNav: (show: boolean) => void
}

export const SignupContainer = ({...props }: SignupContainerProps) => {
  const { setShowNav } = props

  useEffect(() => {
    setShowNav(false)
  }, [setShowNav])

  return (
    <Signup {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { user } = state
  return {
    loggedIn: user.loggedIn
  }
}

export function mapDispatchToProps(dispatch: any) {
  return {
    setShowNav: (show: boolean) => dispatch(setShowNav(show)),
    handleSignUp: signUpWithPassword
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupContainer  )
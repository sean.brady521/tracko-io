import React, { useState } from 'react'
import { IonButton, IonIcon, IonInput, IonItem, IonLabel, IonPage, IonText } from '@ionic/react'
import { mailOutline, wallet } from 'ionicons/icons'
import { Redirect } from 'react-router'
import styles from './signin.module.scss'

export interface SigninProps {
  loggedIn: boolean
  error: string | null
  onSubmit: (email: string, password: string) => Promise<void>
}

const Signin = (props: SigninProps) => {
  const { loggedIn, error, onSubmit } = props

  const [email, setEmail] = useState<string>('')
  const [password, setPassword] = useState<string>('')

  return (
    <IonPage>
      <div className={styles.contentWrap}>
        <div className={styles.headerWrap}>
          <IonIcon icon={wallet} color='primary' className={styles.headerIcon} />
          <IonText color='dark'>
            <div className={styles.header}>
              Tracko
            </div>
          </IonText>
        </div>
        <div className={styles.inputs}>
          <IonItem className={styles.email}>
            <IonLabel position='floating' color='medium'>Email</IonLabel>
            <IonInput
              autofocus
              value={email}
              onIonChange={(e) => setEmail(e.detail.value!)}
            />
          </IonItem>
          <IonItem className={styles.password}>
            <IonLabel position='floating' color='medium'>Password</IonLabel>
            <IonInput
              type='password'
              value={password}
              onIonChange={(e) => setPassword(e.detail.value!)}
            />
          </IonItem>
        </div>
        <div className={styles.loginButtons}>
          <IonButton 
            color='primary' 
            shape='round' 
            expand='full'
            className={styles.emailButton} 
            onClick={() => onSubmit(email, password)}
          >
            <div className={styles.buttonInner}>
              Log in
            </div>
          </IonButton>
        </div>
      </div>
      {loggedIn && <Redirect to='/categories' />}
    </IonPage>
  )
}

export default Signin
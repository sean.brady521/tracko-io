import React from 'react'
import NavBar, { NavBarProps } from './nav-bar'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'

export const NavBarContainer = ({...props }: NavBarProps) => {
  return (
    <NavBar {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { showNav } = state.user

  return {
    shouldHide: !showNav
  }
}

export default connect(mapStateToProps)(NavBarContainer)
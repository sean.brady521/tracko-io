import React, { useEffect } from 'react'
import ManageTrCategory, { ManageTrCategoryProps } from './manage-tr-category'
import { connect } from 'react-redux'
import { ICategory, StoreState } from 'types/common'
import { setShowNav } from 'store/user/user-actions'
import parseLocation from 'utils/parse-location'
import { updateEditingTransaction } from 'store/transactions/transactions-actions'
import { RouteComponentProps } from 'react-router'

interface CategoriesContainerProps extends ManageTrCategoryProps, RouteComponentProps { 
  setShowNav: (show: boolean) => void
}

export const ManageTrCategoryContainer = ({...props }: CategoriesContainerProps) => {
  const { setShowNav } = props

  useEffect(() => {
    setShowNav(false)
  }, [setShowNav])

  return (
    <ManageTrCategory {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { editingTransaction } = state.transactions
  const { categories } = state.categories
  const { tranId } = parseLocation(window.location)

  let backLink = '/edit-transaction'
  if (tranId) backLink += `?tranId=${tranId}`


  return {
    backLink,
    categories,
    defaultCategory: editingTransaction.category,
  }
}

export function mapDispatchToProps(dispatch: any, ownProps: CategoriesContainerProps) {
  const { tranId } = parseLocation(window.location)

  const { history } = ownProps

  return { 
    setShowNav: (show: boolean) => dispatch(setShowNav(show)),
    onSelect: (category: Partial<ICategory>) => {
      dispatch(updateEditingTransaction({
        id: tranId,
        categoryId: category.id
      }))

      let backLink = '/edit-transaction'
      if (tranId) backLink += `?tranId=${tranId}`
      history.push(backLink)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageTrCategoryContainer)